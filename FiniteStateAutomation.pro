#-------------------------------------------------
#
# Project created by QtCreator 2014-01-26T15:25:34
#
#-------------------------------------------------

QT       += core

QT       -= gui

TARGET = FiniteStateAutomation
CONFIG   += console
CONFIG   -= app_bundle

TEMPLATE = app


SOURCES += main.cpp \
    state.cpp \
    token.cpp

HEADERS += \
    state.h \
    token.h
