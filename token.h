#ifndef TOKEN_H
#define TOKEN_H

#include <QString>

class Token
{
public:
  Token(int value = 0);

  int key;
  QString Value;
};

#endif // TOKEN_H
