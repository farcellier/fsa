#ifndef STATE_H
#define STATE_H

#include <QString>
#include <QHash>

#include <exception>
#include <stdexcept>

#include "token.h"

using namespace std;

class State
{
public:
  State(Token);

  void setTransition(QChar, State*);
  Token move(QString input);

private:
  QString label;    // for the debugging purpose.
  QHash<QChar, State*> _nextStates;
  Token _token;
};

#endif // STATE_H
