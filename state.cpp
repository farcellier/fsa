#include "state.h"

State::State(Token finalValue)
{
  this ->_token = finalValue;
}


void State::setTransition(QChar value, State* state)
{
  if (this->_nextStates.contains(value))
  {
      throw new logic_error("Transition already set");
  }

  this->_nextStates[value] = state;
}


Token State::move(QString input)
{
  QChar value = input[0];
  if(this -> _nextStates.contains(value))
  {
      Token token = this -> _nextStates[value]->move(input.right(input.length() - 1));
      token.Value = token.Value.prepend(value);
      return token;
  }
  else
  {
      return _token;
  }
}
