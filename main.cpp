#include <QtCore/QCoreApplication>

#include <QDebug>
#include "state.h"

int main(int argc, char *argv[])
{
  QCoreApplication a(argc, argv);

  QString inputString("a");

  const Token INVALID(0),
      VALID(1);

  State* s0 = new State(INVALID);
  State* s1 = new State(INVALID);
  State* s2 = new State(VALID);
  State* s3 = new State(VALID);

  s0 -> setTransition ('a', s1);
  s1 -> setTransition ('b', s2);
  s2 -> setTransition ('c', s3);
  s3 -> setTransition ('c', s3);

  Token tok = s0 -> move (inputString);
  
  qDebug() << tok.Value;

  return a.exec();
}
